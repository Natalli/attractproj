import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'city'
})
export class CityPipe implements PipeTransform {
    transform(data, value): any {
        if (value) {
            return data.filter(item => {
                return item.city === value;
            });
        } else {
            return data;
        }
    }
}
