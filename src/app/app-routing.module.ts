import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './components/list/list.component';
import {CentralViewComponent} from './components/central-view/central-view.component';
import {HomeComponent} from './page/home/home.component';

const routes: Routes = [

    {
        path: '', component: CentralViewComponent,
        children: [
            {path: '', component: HomeComponent}
        ]
    },
    {path: '', redirectTo: '/', pathMatch: 'full'},
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes,
        {
            useHash: true,
            scrollPositionRestoration: 'enabled'
        })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
