import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  filterData: Object;
  constructor() {
    this.filterData = {city: null, category: [], price: []};
  }

  ngOnInit() {
  }

  filtered(data: {city: number, category: Array<number>, price: Array<number>}) {
    this.filterData = data;
    console.log(this.filterData);
  }

}
