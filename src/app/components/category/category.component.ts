import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {FormGroup, FormBuilder, FormArray, FormControl} from '@angular/forms';
import {CityService} from '../../service/city.service';
import {City} from '../../model/city';
import {CategoryService} from '../../service/category.service';
import {Category} from '../../model/category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less']
})
export class CategoryComponent implements OnInit {
  @Output() Filtered = new EventEmitter<{city: number, category: Array<number>, price: Array<number>}>();
  cities: City[];
  categories: Category[];
  public form: FormGroup;
  min1: number;
  max1: number;
  step: number;
  twoWayRange: any;
  constructor(private formBuilder: FormBuilder,
              private cityService: CityService,
              private categoryService: CategoryService) {
    this.cities = this.cityService.getCity();
    this.categories = this.categoryService.getCategories();
    this.min1 = 0;
    this.max1 = 250;
    this.step = 5;
    this.twoWayRange = [0, 250];
  }

  ngOnInit() {
    this.formFilter();
  }

  formFilter() {
    this.form = this.formBuilder.group({
      city: [],
      category: new FormArray([]),
      price: ['']
    });
  }

  onCheckChange(event) {
    const formArray: FormArray = this.form.get('category') as FormArray;
    if (event.target.checked) {
      formArray.push(new FormControl(+event.target.value));
    } else {
      let i = 0;
      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === event.target.value) {
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  filter() {
    this.Filtered.emit({
      city: +this.form.value.city,
      category: this.form.value.category,
      price: this.form.value.price
    });
  }
}

