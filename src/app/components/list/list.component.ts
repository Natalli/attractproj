import {Component, Input, OnInit} from '@angular/core';
import {Data} from '../../model/data';
import {ApiService} from '../../service/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  @Input('filterData') filter: {city: number, category: Array<number>, price: Array<number>};

  data: Data[];

  constructor(private apiService: ApiService) {
    this.data = [];
  }

  ngOnInit() {
    this.data = this.apiService.getData();
  }
}
