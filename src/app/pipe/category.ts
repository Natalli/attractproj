import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {
  transform(data, values: Array<string>): any {
    if (values.length) {
      const newArr = [];
      values.forEach(value => {
        const returnObjects = data.filter(item => item.category === value);
        newArr.push(...returnObjects);
      });
      return newArr;
    } else {
      return data;
    }
  }
}
